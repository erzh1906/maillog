import logging


def check_bad_from(data):

    if data['sender'] == 'not_found_from' and data['sender_domain'] == 'not_found_domain':
        return True
    else:
        return False


def write_new_mail(data, session, mail_table, mail_recipient):
    mail = mail_table(
        mail_id=data['id'],
        date=data['date'],
        source=data['source'],
        sender=data['sender'],
        sender_domain=data['sender_domain'],
        sended_count=data['sended_count'],
        subject=data['subject'],
        chain=data['chain']
    )
    mail.mail_mailrecipient_collection = []
    for el in data['to']:
        logging.debug('RCPT: %s added recipient %s with %s mx %s status at %s' % (
            data['id'],
            el['recipient'],
            el['server'],
            el['status'],
            el['received']
        ))
        mail.mail_mailrecipient_collection.append(
            mail_recipient(
                recipient=el['recipient'],
                recipient_domain=el['recipient_domain'],
                server=el['server'],
                status=el['status'],
                message=el['message'],
                received=el['received']
            )
        )
    session.add(mail)
    try:
        session.commit()
    except:
        logging.exception("Couldn't write %s" % (str(data['id'])))


def write_all_recipients(mail, data, mail_recipient):
    mail.mail_mailrecipient_collection = []
    for el in data['to']:
        logging.debug('RCPT: %s added recipient %s with %s mx %s status at %s' % (
            data['id'],
            el['recipient'],
            el['server'],
            el['status'],
            el['received']
        ))
        mail.mail_mailrecipient_collection.append(
            mail_recipient(
                recipient=el['recipient'],
                recipient_domain=el['recipient_domain'],
                server=el['server'],
                status=el['status'],
                message=el['message'],
                received=el['received']
            )
        )


def write(data, session, mail_table, mail_recipient):

    check = check_bad_from(data=data)
    x = session.query(mail_table)\
        .filter(mail_table.mail_id == data['id'])\
        .filter(mail_table.source == data['source'])\
        .filter(mail_table.date == data['date'])\
        .filter(mail_table.sender == data['sender'])

    if session.query(x.exists()).scalar() is False:
        logging.debug('NEW: %s in %s from %s' % (data['id'], data['source'], data['sender']))
        write_new_mail(data=data, session=session, mail_table=mail_table, mail_recipient=mail_recipient)
    else:
        mail = x.first()
        if check is True:
            pass
        else:
            y = session.query(mail_table) \
                .filter(mail_table.mail_id == data['id']) \
                .filter(mail_table.source == data['source']) \
                .filter(mail_table.sender == data['sender']) \
                .filter(mail_table.sender_domain == data['sender_domain']) \
                .filter(mail_table.date == data['date'])

            if session.query(y.exists()).scalar() is False:
                logging.debug('UPDATE: %s in %s sender to %s' % (data['id'], data['source'], data['sender']))
                mail.sender = data['sender']
                mail.sender_domain = data['sender_domain']
            else:
                logging.debug(
                    'EXIST: %s in %s from %s at %s' % (data['id'], data['source'], data['sender'], data['date'])
                )
        for el in data['to']:
            rcpt_check = session.query(mail_recipient).filter(
                mail_recipient.mail_id_id == mail.id,
                mail_recipient.recipient == el['recipient'],
                mail_recipient.server == el['server'],
                mail_recipient.received == el['received']
            )
            if session.query(rcpt_check.exists()).scalar() is True:
                logging.debug('RCPT: %s of %s already exist' % (el['recipient'], data['id']))
            else:
                logging.debug('RCPT: %s added recipient %s with %s mx %s status at %s' % (
                    data['id'],
                    el['recipient'],
                    el['server'],
                    el['status'],
                    el['received']
                ))
                mail.mail_mailrecipient_collection.append(
                    mail_recipient(
                            recipient=el['recipient'],
                            recipient_domain=el['recipient_domain'],
                            server=el['server'],
                            status=el['status'],
                            message=el['message'],
                            received=el['received']
                        )
                    )
        mail.sended_count = len(mail.mail_mailrecipient_collection)
        session.add(mail)
        try:
            session.commit()
        except:
            logging.exception("Couldn't write %s" % (str(data['id'])))
