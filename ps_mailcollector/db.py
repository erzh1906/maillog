from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy.ext.automap import automap_base


class StatDB(object):

    def __init__(self, username, password, database, host, port):
        self.username = username
        self.password = password
        self.database = database
        self.host = host
        self.port = port

    def db_connect(self):
        db_url = 'postgresql://{}:{}@{}:{}/{}'
        db_url = db_url.format(self.username, self.password, self.host, self.port, self.database)
        base = automap_base()
        connection = create_engine(db_url, client_encoding='utf8')
        session = Session(connection)
        base.prepare(connection, reflect=True)
        return base, session
