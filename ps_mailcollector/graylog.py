import re
import requests
import fileinput


class Graylog(object):
    """
    Graylog class
    Fetch data from graylog-server and write in file
    """

    def __init__(self, api_url, api_token, api_file, api_source, replace_list, timerange):
        self.api_url = api_url
        self.api_token = api_token
        self.api_file = api_file
        self.api_source = api_source
        self.replace_list = replace_list
        self.timerange = timerange

    def fetch_data(self):
        data = {}
        data['query'] = self.api_source
        data['fields'] = 'message'
        data['range'] = str(self.timerange)
        req = requests.get(
            self.api_url,
            auth=requests.auth.HTTPBasicAuth(self.api_token, 'token'),
            params=data
        )
        return req

    def write_log(self, req):
        with open(self.api_file, "w") as file_desc:
            file_desc.write(req.text)

    def read_log(self):
        with open(self.api_file, 'r') as file_desc:
            lines = file_desc.readlines()

        return lines

    def replace_ips(self):
        for node in self.replace_list:
            with fileinput.FileInput(self.api_file, inplace=True) as file:
                textToSearch = '%s postfix/' % (node)
                textToReplace = '%s postfix/' % (self.replace_list[node])
                for line in file:
                    print(line.replace(textToSearch, textToReplace), end='')


def serialize_log(log):
    """
    Create dict from log file
    Returns dict of multiple dicts like
    'F38EB7492D': {
        'date': '2017-01-02T09:33:52.000Z',
        'id': 'F38EB7492D',
        'nrcpt': 1,
        'sender': 'office_t@neocar.kz',
        'sender_domain': 'neocar.kz',
        'source': 'mailgate09.ps.kz',
        'to': [
            {
                'message': 'connect to mailgate03.ps.kz[91.201.215.68]:25: No route to host',
                'recipient': 'MAILER-DAEMON@mailgate03.ps.kz',
                'recipient_domain': 'mailgate03.ps.kz',
                'server': 'none',
                'status': 'deferred'
            },
            {
                'message': 'connect to mailgate03.ps.kz[91.201.215.68]:25: No route to host',
                'recipient': 'MAILER-DAEMON@mailgate03.ps.kz',
                'recipient_domain': 'mailgate03.ps.kz',
                'server': 'none',
                'status': 'deferred'
            },
            {
                'message': 'connect to mailgate03.ps.kz[91.201.215.68]:25: No route to host',
                'recipient': 'MAILER-DAEMON@mailgate03.ps.kz',
                'recipient_domain': 'mailgate03.ps.kz',
                'server': 'none',
                'status': 'deferred'
            }
        ]
    }
    """

    from_pattern = r'"(\S+)","(\S+) .*: ([A-Z0-9]+): from=<(\B|\S+)>, .* nrcpt=(\S+)'
    regex_from = re.compile(from_pattern)

    to_pattern = r'"(\S+)","(\S+) .*: ([A-Z0-9]+): to=<(\S+)>,.*relay=(\S+), .* status=(\S+) \((.*)\)'
    regex_to = re.compile(to_pattern)

    subject_pattern = r'"(\S+)","(\S+) .*: ([A-Z0-9]+): warning: header Subject: (.*) from relay.*; from=<(\B|\S+)>'
    regex_subject = re.compile(subject_pattern)

    chain_pattern = r'"(\S+)","(\S+) .*: ([A-Z0-9]+): warning: header Received: (.*) from relay.*; from=<(\B|\S+)>'
    regex_chain = re.compile(chain_pattern)

    result = {}
    for line in log:
        match_from = regex_from.search(line)
        match_subject = regex_subject.search(line)
        match_to = regex_to.search(line)
        match_chain = regex_chain.search(line)
        if match_from:
            if match_from.group(4) != '':
                sender = match_from.group(4)[:255]
                sender_domain = match_from.group(4).split('@')[1]
            else:
                sender = 'empty_from'
                sender_domain = 'empty_domain'
            if match_from.group(3) in result:
                result[match_from.group(3)]["sender"] = sender
                result[match_from.group(3)]["sender_domain"] = sender_domain
            else:
                result[match_from.group(3)] = {
                    "date": match_from.group(1),
                    "source": match_from.group(2),
                    "id": match_from.group(3),
                    "sender": sender,
                    "sender_domain": sender_domain,
                    "sended_count": 0,
                    "subject": "",
                    "chain": "",
                    "to": []
                }
        else:
            pass
        if match_subject:
            if match_subject.group(5) != '':
                sender = match_subject.group(5)[:255]
                sender_domain = match_subject.group(5).split('@')[1]
            else:
                sender = 'empty_from'
                sender_domain = 'empty_domain'
            if match_subject.group(3) in result:
                result[match_subject.group(3)]["sender"] = sender
                result[match_subject.group(3)]["sender_domain"] = sender_domain
                result[match_subject.group(3)]["subject"] = match_subject.group(4)
            else:
                result[match_subject.group(3)] = {
                    "date": match_subject.group(1),
                    "source": match_subject.group(2),
                    "id": match_subject.group(3),
                    "sender": sender,
                    "sender_domain": sender_domain,
                    "sended_count": 0,
                    "subject": match_subject.group(4),
                    "chain": "",
                    "to": []
                }
        else:
            pass
        if match_chain:
            if match_chain.group(5) != '':
                sender = match_chain.group(5)[:255]
                sender_domain = match_chain.group(5).split('@')[1]
            else:
                sender = 'empty_from'
                sender_domain = 'empty_domain'
            if match_chain.group(3) in result:
                result[match_chain.group(3)]["sender"] = sender
                result[match_chain.group(3)]["sender_domain"] = sender_domain
                result[match_chain.group(3)]["chain"] = result[match_chain.group(3)]["chain"] + match_chain.group(4)
            else:
                result[match_chain.group(3)] = {
                    "date": match_chain.group(1),
                    "source": match_chain.group(2),
                    "id": match_chain.group(3),
                    "sender": sender,
                    "sender_domain": sender_domain,
                    "sended_count": 0,
                    "subject": "",
                    "chain": match_chain.group(4),
                    "to": []
                }
        else:
            pass
        if match_to:
            if match_to.group(3) in result:
                result[match_to.group(3)]["to"].append(
                    {
                        "recipient": match_to.group(4)[:255],
                        "recipient_domain": match_to.group(4).split('@')[1],
                        "server": match_to.group(5),
                        "status": match_to.group(6),
                        "message": match_to.group(7),
                        "received": match_to.group(1)
                    }
                )
                result[match_to.group(3)]['sended_count'] = result[match_to.group(3)]['sended_count'] + 1
            else:
                result[match_to.group(3)] = {
                    "date": match_to.group(1),
                    "source": match_to.group(2),
                    "id": match_to.group(3),
                    "sender": "not_found_from",
                    "sender_domain": "not_found_domain",
                    "sended_count": 1,
                    "subject": "",
                    "chain": "",
                    "to": [
                        {
                            "recipient": match_to.group(4)[:255],
                            "recipient_domain": match_to.group(4).split('@')[1],
                            "server": match_to.group(5),
                            "status": match_to.group(6),
                            "message": match_to.group(7),
                            "received": match_to.group(1)
                        }
                    ]
                }
        else:
            pass

    return result
