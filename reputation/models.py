from django.db import models

# Create your models here.


class Domain(models.Model):
    class Meta:
        verbose_name = 'Домен'
        verbose_name_plural = 'Домены'

    name = models.CharField(unique=True, db_index=True, max_length=255)
    last_check = models.DateTimeField()
    last_total_count = models.PositiveIntegerField()
    last_bounced_count = models.PositiveIntegerField()
    last_deferred_count = models.PositiveIntegerField()
    bounce_rate = models.PositiveIntegerField(db_index=True)
    deferred_rate = models.PositiveIntegerField(db_index=True)
    blocked = models.BooleanField(default=False)
    extended_rate = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Notice(models.Model):
    class Meta:
        verbose_name = 'Примечание'
        verbose_name_plural = 'Примечания'

    domain = models.ForeignKey(Domain, related_name='notices')
    notice = models.TextField()

    def __str__(self):
        return self.notice
