from django.contrib import admin
from reputation.models import Domain, Notice

# Register your models here.


admin.site.register(Domain)
admin.site.register(Notice)
