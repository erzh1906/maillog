from django.conf.urls import url, include
from rest_framework import routers
from mail import views


router = routers.DefaultRouter()
router.register(r'mails', views.MailViewSet)
router.register(r'mailrecipients', views.MailRecipientViewSet)
router.register(r'senders', views.SenderTop, base_name='senders')
router.register(r'recipients', views.RecipientTop, base_name='recipients')
router.register(r'graph', views.RawGraph, base_name='graph')

app_name = 'mail'
urlpatterns = [
    url(
        r'^$',
        views.HomeView.as_view(),
        name='home'
    ),
    url(
        r'^graph/',
        views.GraphView.as_view(),
        name='graph'
    ),
    url(
        r'^api/',
        include(router.urls),
    ),
    url(
        r'^mailru/',
        views.mail_ru,
        name='mailru'
    ),
    url(
        r'^gmailcom/',
        views.gmail_com,
        name='gmail'
    ),
    url(
        r'^total/',
        views.total_count,
        name='total'
    )
]
