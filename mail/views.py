import pytz
import django_filters.rest_framework
from datetime import datetime, timedelta
from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination, Response
from django.db.models import Q, Sum, Count, DateTimeField
from django.db.models.functions import TruncDate, TruncHour
from django.views import generic
from django.http import HttpResponse, JsonResponse
from mail.models import Mail, MailRecipient
from mail.serializers import MailSerializer, MailDetailSerializer, MailRecipientSerializer, TopSerializer
from mailstat.settings import TIME_ZONE


class VoidPagination(PageNumberPagination):
    page_size = 100000
    page_size_query_param = 'page'
    max_page_size = 100000

    def get_paginated_response(self, data):
        return Response(data)


class MailViewSet(viewsets.ModelViewSet):
    queryset = Mail.objects.all().order_by('-id')
    filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
    filter_fields = ('mail_id', 'date', 'sender', 'source')

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return MailDetailSerializer
        else:
            return MailSerializer


class MailRecipientViewSet(viewsets.ModelViewSet):
    queryset = MailRecipient.objects.all().order_by('id')
    serializer_class = MailRecipientSerializer


class SenderTop(viewsets.ModelViewSet):
    serializer_class = TopSerializer
    pagination_class = None

    def get_queryset(self):
        query_attributes = {}
        now = datetime.now()
        start = self.request.query_params.get('start', None)
        end = self.request.query_params.get('end', None)
        field = self.request.query_params.get('type', 'sender')
        source = self.request.query_params.get('source', None)
        hop = self.request.query_params.get('hop', 1)
        sender = self.request.query_params.get('sender', None)
        sender_domain = self.request.query_params.get('sender_domain', None)
        subject = self.request.query_params.get('subject', None)
        recipient = self.request.query_params.get('recipient', None)
        recipient_domain = self.request.query_params.get('recipient_domain', None)
        message = self.request.query_params.get('message', None)
        server = self.request.query_params.get('server', None)
        status = self.request.query_params.get('status', None)

        if start is None:
            start_time = now - timedelta(hours=int(hop))
        else:
            start_time = DateTimeField().to_python(start)
        if end is None:
            end_time = now
        else:
            end_time = DateTimeField().to_python(end)

        date__range = (start_time, end_time)
        query_attributes['date__range'] = date__range

        if source:
            query_attributes['source'] = source
        if sender:
            query_attributes['sender'] = sender
        if sender_domain:
            query_attributes['sender_domain'] = sender_domain
        if subject:
            query_attributes['subject__icontains'] = subject
        if recipient:
            query_attributes['recipients__recipient'] = recipient
        if recipient_domain:
            query_attributes['recipients__recipient_domain'] = recipient_domain
        if message:
            query_attributes['recipients__message__icontains'] = message
        if server:
            query_attributes['recipients__server__icontains'] = server
        if status:
            query_attributes['recipients__status'] = status

        queryset = Mail.objects.filter(**query_attributes)

        if recipient or recipient_domain or message or server or status:
            queryset = queryset.extra({'object': field}).\
                           values('object').\
                           annotate(count=Count(field)).\
                           order_by('-count')[:20]
        else:
            queryset = queryset.extra({'object': field}).\
                           values('object').\
                           annotate(count=Sum('sended_count')).\
                           order_by('-count')[:20]
        return queryset


class RecipientTop(viewsets.ModelViewSet):
    serializer_class = TopSerializer
    pagination_class = None

    def get_queryset(self):
        query_attributes = {}
        now = datetime.now()
        start = self.request.query_params.get('start', None)
        end = self.request.query_params.get('end', None)
        field = self.request.query_params.get('type', 'recipient')
        source = self.request.query_params.get('source', None)
        hop = self.request.query_params.get('hop', 1)
        sender = self.request.query_params.get('sender', None)
        sender_domain = self.request.query_params.get('sender_domain', None)
        subject = self.request.query_params.get('subject', None)
        recipient = self.request.query_params.get('recipient', None)
        recipient_domain = self.request.query_params.get('recipient_domain', None)
        message = self.request.query_params.get('message', None)
        server = self.request.query_params.get('server', None)
        status = self.request.query_params.get('status', None)

        if start is None:
            start_time = now - timedelta(hours=int(hop))
        else:
            start_time = DateTimeField().to_python(start)
        if end is None:
            end_time = now
        else:
            end_time = DateTimeField().to_python(end)

        date__range = (start_time, end_time)
        query_attributes['mail_id__date__range'] = date__range

        if source:
            query_attributes['mail_id__source'] = source
        if sender:
            query_attributes['mail_id__sender'] = sender
        if sender_domain:
            query_attributes['mail_id__sender_domain'] = sender_domain
        if subject:
            query_attributes['mail_id__subject'] = subject
        if recipient:
            query_attributes['recipient'] = recipient
        if recipient_domain:
            query_attributes['recipient_domain'] = recipient_domain
        if message:
            query_attributes['message__icontains'] = message
        if server:
            query_attributes['server__icontains'] = server
        if status:
            query_attributes['status'] = status

        queryset = MailRecipient.objects.filter(**query_attributes).extra({'object': field}).\
            values('object').\
            annotate(count=Count(field)).\
            order_by('-count')[:20]
        return queryset


class RawGraph(viewsets.ModelViewSet):
    serializer_class = TopSerializer
    pagination_class = None

    def get_queryset(self):
        query_attributes = {}
        now = datetime.now()
        tz = pytz.timezone(TIME_ZONE)
        start = self.request.query_params.get('start', None)
        end = self.request.query_params.get('end', None)
        sender = self.request.query_params.get('sender', None)
        recipient = self.request.query_params.get('recipient', None)
        source = self.request.query_params.get('source', None)
        server = self.request.query_params.get('server', None)
        message = self.request.query_params.get('message', None)
        status = self.request.query_params.get('status', None)
        graph_type = self.request.query_params.get('graph_type', 'hourly')

        if graph_type == 'hourly':
            trunc_function = TruncHour
            delta = timedelta(hours=96)
        if graph_type == 'dayly':
            trunc_function = TruncDate
            delta = timedelta(days=28)
        if start is None or start == '':
            start_time = now - delta
        else:
            start_time = DateTimeField().to_python(start)
        if end is None or end == '':
            end_time = now
        else:
            end_time = DateTimeField().to_python(end)

        date__range = (start_time, end_time)
        query_attributes['date__range'] = date__range

        if sender:
            query_attributes['sender__icontains'] = sender
        if recipient:
            query_attributes['recipients__recipient__icontains'] = recipient
        if source:
            query_attributes['source'] = source
        if server:
            query_attributes['recipients__server__icontains'] = server
        if message:
            query_attributes['recipients__message__icontains'] = message
        if status:
            query_attributes['recipients__status'] = status

        queryset = Mail.objects.filter(**query_attributes)
        if recipient or server or message or status:
            queryset = queryset.\
                annotate(object=trunc_function('date', tzinfo=tz)).\
                values('object').\
                annotate(count=Count('object')).\
                order_by('object')
        else:
            queryset = queryset.\
                annotate(object=trunc_function('date', tzinfo=tz)).\
                values('object').\
                annotate(count=Sum('sended_count')).\
                order_by('object')
        return queryset


def mail_ru(request):
    now = datetime.now()
    end_time = now
    start_time = now - timedelta(hours=1)
    queryset = MailRecipient.objects.filter(
        recipient_domain__in=['mail.ru', 'list.ru', 'bk.ru', 'inbox.ru'],
        received__range=(start_time, end_time)
    )
    total = queryset.count()
    bounced = queryset.filter(status='bounced').count()
    dmarc = queryset.filter(status='bounced', message__icontains='dmarc').count()
    spam = queryset.filter(status='bounced', message__icontains='spam').count()
    invalid = queryset.filter(status='bounced', message__icontains='invalid').count()
    return JsonResponse(
        {
            'total': total,
            'bounced': bounced,
            'dmarc': dmarc,
            'spam': spam,
            'invalid': invalid
        }
    )


def gmail_com(request):
    now = datetime.now()
    end_time = now
    start_time = now - timedelta(hours=1)
    queryset = MailRecipient.objects.filter(
        recipient_domain='gmail.com',
        received__range=(start_time, end_time)
    )
    total = queryset.count()
    bounced = queryset.filter(status='bounced').count()
    dmarc = queryset.filter(status='bounced', message__icontains='dmarc').count()
    rateperm = queryset.filter(status='bounced', message__icontains='ReceivingRatePerm').count()
    issued = queryset.filter(status='bounced', message__icontains='security issue').count()
    invalid = queryset.filter(status='bounced', message__icontains='does not exist').count()
    return JsonResponse(
        {
            'total': total,
            'bounced': bounced,
            'dmarc': dmarc,
            'issued': issued,
            'invalid': invalid,
            'rateperm': rateperm
        }
    )


def total_count(request):
    now = datetime.now()
    end_time = now
    start_time = now - timedelta(hours=1)
    queryset = MailRecipient.objects.filter(
        received__range=(start_time, end_time)
    )
    total = queryset.count()
    bounced = queryset.filter(status='bounced').count()
    return JsonResponse(
        {
            'total': total,
            'bounced': bounced
        }
    )


class HomeView(generic.TemplateView):
    template_name = 'mail/index.html'


class GraphView(generic.TemplateView):
    template_name = 'mail/graphs.html'
