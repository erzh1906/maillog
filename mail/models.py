from django.db import models
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField

# Create your models here.


class Mail(models.Model):

    class Meta:
        verbose_name = 'Mail'
        verbose_name_plural = 'Mails'
        unique_together = (('mail_id', 'source', 'sender', 'date'),)

    sended_count = models.PositiveIntegerField()
    mail_id = models.CharField(max_length=255)
    source = models.CharField(max_length=255)
    sender = models.CharField(max_length=255, null=True, db_index=True)
    sender_domain = models.CharField(max_length=255, null=True, db_index=True)
    date = models.DateTimeField(db_index=True)
    subject = models.TextField(blank=True, null=True)
    chain = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.mail_id


class MailRecipient(models.Model):

    class Meta:
        verbose_name = 'Mail recipient'
        verbose_name_plural = 'Mail recipients'
        unique_together = (('mail_id', 'recipient', 'server', 'received'),)
        indexes = [GinIndex(fields=['sv'])]

    mail_id = models.ForeignKey(Mail, related_name='recipients')
    recipient = models.CharField(max_length=255, null=True, db_index=True)
    recipient_domain = models.CharField(max_length=255, null=True, db_index=True)
    server = models.CharField(max_length=255, null=True)
    status = models.CharField(max_length=255, null=True)
    message = models.TextField(db_index=True)
    received = models.DateTimeField(db_index=True)
    sv = SearchVectorField(blank=True, null=True)

    def __str__(self):
        return self.recipient
