from rest_framework import serializers
from mail.models import Mail, MailRecipient


class MailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Mail
        fields = [
            'id',
            'mail_id',
            'date',
            'source',
            'sender',
            'sender_domain',
            'subject',
            'chain'
        ]


class MailRecipientSerializer(serializers.ModelSerializer):

    class Meta:
        model = MailRecipient
        fields = [
            'id',
            'mail_id',
            'recipient',
            'recipient_domain',
            'server',
            'status',
            'message'
        ]


class MailRecipientDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = MailRecipient
        fields = [
            'recipient',
            'recipient_domain',
            'server',
            'status',
            'message'
        ]


class MailDetailSerializer(serializers.ModelSerializer):

    recipients = MailRecipientDetailSerializer(many=True, read_only=True)

    class Meta:
        model = Mail
        fields = [
            'id',
            'mail_id',
            'date',
            'source',
            'sender',
            'sender_domain',
            'subject',
            'chain',
            'recipients'
        ]


class TopSerializer(serializers.Serializer):
    object = serializers.CharField(read_only=True)
    count = serializers.IntegerField(read_only=True)
