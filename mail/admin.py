from django.contrib import admin
from mail.models import Mail, MailRecipient


class MailRecipientTabular(admin.TabularInline):
    model = MailRecipient
    extra = 0


class MailRecipientAdmin(admin.ModelAdmin):
    model = MailRecipient
    list_display = ['mail_id', 'recipient', 'recipient_domain', 'server', 'status', 'message']
    search_fields = ['recipient', 'recipient_domain', 'server', 'message']
    list_filter = ['status']


class MailAdmin(admin.ModelAdmin):
    model = Mail
    list_display = ['mail_id', 'date', 'sender', 'sender_domain', 'sended_count']
    search_fields = ['mail_id', 'sender']
    list_filter = ['sended_count']
    inlines = [MailRecipientTabular]


admin.site.register(Mail, MailAdmin)
admin.site.register(MailRecipient, MailRecipientAdmin)
