#!/usr/bin/env python

import argparse
import logging
from sys import exit
from time import sleep
from pathlib import Path

from ps_mailcollector import config
from ps_mailcollector.db import StatDB
from ps_mailcollector.graylog import Graylog, serialize_log
from ps_mailcollector.collector import write


delay_multiplier = 1

logging.basicConfig(
    level=config.LOG_LEVEL,
    filename=config.LOG_FILE,
    format='[%(asctime)s] # %(filename)-20s # %(levelname)-8s # %(message)s'
)


def check_previous_process(file):
    result = Path(file).is_file()
    return result


def main(delay=0, delay_multiplier=0):
    description = "Collect data from graylog and write to database"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        '-d', dest='delta',
        help='Time delta to fetch',
        type=int,
        default=1,
    )
    parser.add_argument(
        '-m', dest='multiplier',
        help='Time delta multiplier (seconds)',
        type=int,
        default=60
    )
    args = parser.parse_args()

    logging.info('Job started')

    process_check = check_previous_process(file=config.API_FILE)

    while process_check is not False:
        logging.error('Previous process still running')
        delay_multiplier = delay_multiplier + 1
        sleep(config.DELAY)
        main(delay=config.DELAY, delay_multiplier=delay_multiplier)

    timerange = args.multiplier * args.delta + delay_multiplier * delay

    graylog = Graylog(
        api_url=config.API_URL,
        api_token=config.API_TOKEN,
        api_source=config.API_SOURCE,
        api_file=config.API_FILE,
        replace_list=config.REPLACE_LIST,
        timerange=timerange
    )
    db = StatDB(
        username=config.DB_USER,
        password=config.DB_PASS,
        database=config.DB_NAME,
        host=config.DB_HOST,
        port=config.DB_PORT
    )

    try:
        req = graylog.fetch_data()
    except:
        logging.exception('No data from %s' % (str(config.API_URL)))
        config.gzip_log()
        exit()

    try:
        base, session = db.db_connect()
    except:
        logging.exception('Database connection error')
        config.gzip_log()
        exit()

    graylog.write_log(req=req)
    graylog.replace_ips()
    log = graylog.read_log()
    data = serialize_log(list(set(log)))
    del(log)
    mail_table = base.classes.mail_mail
    mail_recipient = base.classes.mail_mailrecipient

    queue = data.values()
    del(data)

    logging.info('MAIL: Start writing mails')

    for el in queue:
        logging.debug('%s' % (str(el)))
        write(data=el, session=session, mail_table=mail_table, mail_recipient=mail_recipient)

    logging.info('MAIL: End writing mails')
    logging.info('Job ended')
    config.gzip_log()
    config.clear_tmp()
    exit()


if __name__ == "__main__":
    main()
